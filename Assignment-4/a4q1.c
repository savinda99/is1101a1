//calculate the sum and average of any given three integers
#include <stdio.h>

int main(){
	int num=0,total=0,count=0;
	float avg=0;

	for(count=1; count<=3; count++){
		printf("Enter an Integer: ");
		scanf("%d", &num);
		total+=num;
	}
	avg=total/count;

	printf("Total: %d\n", total);
	printf("Average: %f\n", avg);

	return 0;
}

