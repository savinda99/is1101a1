//find the volume of the cone where the user inputs the height and radius
#include <stdio.h>
int main(){
	float pi=3.141592,r=0,h=0;
	
	printf("Enter the height: ");
	scanf("%f", &h);
	printf("Enter the radius: ");
	scanf("%f", &r);

	printf("%f\n", pi*r*r*h/3);
	return 0;
}
