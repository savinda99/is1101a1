//swap two numbers
#include <stdio.h>
int main(){
	int a=0,b=0;
	printf("Enter two integers: ");
	scanf("%d %d", &a,&b);
	
	printf("A=%d\nB=%d\n",a,b);
	a=a+b;
	b=a-b;
	a=a-b;
	printf("Swapping numbers...\n");
	printf("A=%d\nB=%d\n",a,b);
	return 0;
}
