//convert temperature given in celsius to fahrenheit
#include <stdio.h>
int main(){
	float c=0,f=0;

	printf("Enter temperature in celcius: ");
	scanf("%f", &c);
	f=(c*9/5)+32;
	printf("%.2f°C = %.2f°F\n", c,f);
	return 0;
}
