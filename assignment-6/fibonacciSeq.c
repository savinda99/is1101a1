//a recursive function fibonacciSeq(n) to print the fibonacci sequence up to n
#include <stdio.h>

void fibonacciSeq(int);
int fibonacciNum(int);

int fibonacciNum(int n){
    if (n==0) return 0;
    else if (n==1) return 1;
    else return fibonacciNum(n-1)+fibonacciNum(n-2);
}

void fibonacciSeq(int n){
    static int x=0,y=0;
    if (n>=y){
        printf("%d\n", fibonacciNum(x));
        x++;
        y++;
        fibonacciSeq(n);
    } else return 0;
}

int main(){
    int n;
    printf("Enter the number: ");
    scanf("%d",&n);
    fibonacciSeq(n);
  return 0;
 }
