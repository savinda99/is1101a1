//a recursive function pattern(n) which prints a number triangle.
#include <stdio.h>

void pattern(int n);
void rowPattern(int);

void pattern(int n){
    static int x=1;
    if (n>0){
        rowPattern(x);
        printf("\n");
        x++;
        pattern(n-1);
    }
}

void rowPattern (int n){
    if (n>0){
        printf("%d",n);
        rowPattern(n-1);
    }
}

int main()
{
    int num=0;
    printf("Enter a number: ");
    scanf("%d", &num);
    pattern(num);
    return 0;
}
