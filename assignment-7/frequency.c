//find the frequence of given character
#include <stdio.h>
#define size 100

void frequence(char freq []);

void frequence(char freq []){
    int i=1,j=0;
    for (i=1; freq[i]!='\0'; i++){
        if (freq[i]==freq[0]) j++;
    }
    printf("Frequency of %c is %d", freq[0],j);
}

int main()
{
    char sent[size];
    printf("Enter a letter and a sentence: ");
    fgets(sent,size,stdin);
    frequence(sent);
    return 0;
}


