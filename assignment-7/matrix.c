//add and multiply two matrixes
#include <stdio.h>
#define rows 10
#define cols 10

void add();
void multiply();
void read();
void print();

int r1,r2,c1,c2;
int addm[rows][cols];
int multim[rows][cols];

int main()
{
    int one[rows][cols];
    int two[rows][cols];
    printf("Enter the number of rows and columns of the first matrix: ");
    scanf("%d %d", &r1,&c1);
    printf("Enter the number of rows and columns of the second matrix: ");
    scanf("%d %d", &r2,&c2);

    read(one, r1, c1);
    read(two, r2, c2);

    if (r1==r2 && c1==c2) add(one,two, r1, c1);
    else printf("Matrix dimensions should be equal to add\n\n");

    if (c1==r2) multiply(one,two, r1, c1, c2);
    else printf("Number of columns in the first matrix should be equal to the number of rows in the second matrix to multiply\n\n");
    return 0;
}

void read(int m[rows][cols], int r, int c){
    printf("Enter matrix:\n");
    for (int i=0; i<r; i++)
     for (int j=0; j<c; j++)
      scanf("%d", &m[i][j]);
      printf("\n");
}

void print(int m[rows][cols], int r, int c){
    for (int i=0; i<r; i++){
     for (int j=0; j<c; j++){
      printf("%d ", m[i][j]);
     }printf("\n");
    }printf("\n");
}

void add(int m1[rows][cols], int m2[rows][cols], int r1, int c1){
    printf("Addition:\n");
    for (int i=0; i<r1; i++){
     for (int j=0; j<c1; j++){
      addm[i][j] = m1[i][j] + m2[i][j];
     }
    }
    print(addm, r1, c1);
}

void multiply(int m1[rows][cols], int m2[rows][cols], int r1, int c1, int c2){
    printf("Multiplication:\n");
    for (int i=0; i<r1; i++){
     for (int j=0; j<c1; j++){
      for (int k=0; k<c2; k++){
      multim[i][j] += m1[i][k] * m2[k][j];
      }
     }
    }
    print(multim, r1, c2);
}




