//reverse the inputted sentence.
#include <stdio.h>
#include <string.h>
#define size 100

void reverse (char rev[]);

void reverse(char rev[]){
    int length=0, i=0;
    length = strlen(rev);
    printf("Reversed sentence: ");
    for (i=length-1; i>=0; i--) printf("%c",rev[i]);
}

int main()
{
    char sent[size];
    printf("Enter a sentence: ");
    fgets(sent,size,stdin);
    reverse(sent);
    return 0;
}


