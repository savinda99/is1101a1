#include <stdio.h>
#define size 5

void getstudent();
void printstudent();

struct student {
    char name[15];
    char subject[25];
    int marks;
};

struct student std[size];

int main()
{
    getstudent();
    printstudent();
    return 0;
}

void getstudent (){
    int i=0;
    for (i=0; i<size; i++){
    printf("Enter student name: ");
    scanf("%s", &std[i].name);
    printf("Enter subject: ");
    scanf(" %[^\n]*%c", &std[i].subject);
    printf("Enter marks: ");
    scanf("%d", &std[i].marks);
    printf("\n");
    }
}

void printstudent (){
    int i=0;
    for (i=0; i<size; i++){
    printf("Student name: %s\n", std[i].name);
    printf("Subject name: %s\n", std[i].subject);
    printf("Marks: %d\n", std[i].marks);
    printf("\n");
    }
}
