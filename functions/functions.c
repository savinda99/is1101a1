//know the exact relationship between profit and ticket price.
#include <stdio.h>

//declaring functions
int cost(int);
int attendance(int);
int revenue(int);
int profit(int);

//calculate the cost
int cost (int c){
    return 500+attendance(c)*3;
}

//calculate the attendance
int attendance(int a){
    return 120+((15-a)*4);
}

//calculate the revenue
int revenue (int r){
    return attendance(r)*r;
}

//calculate the profit
int profit(int p){
    return revenue(p)-cost(p);
}

int main()
{
    int tp;
    printf("Enter ticket price: Rs. ");
    scanf("%d", &tp);
    printf("Profit: Rs.%d\n", profit(tp));
    return 0;
}
